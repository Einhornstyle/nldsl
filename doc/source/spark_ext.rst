.. NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
..
.. NLDSL is licensed under a
.. Creative Commons Attribution-NonCommercial 3.0 Unported License.
..
.. You should have received a copy of the license along with this
.. work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

Spark Extension
***************

.. automodule:: nldsl.spark_extension


Grammar Rules
=============

.. autofunction:: nldsl.spark_extension.expression_only

.. autofunction:: nldsl.spark_extension.on_dataframe

.. autofunction:: nldsl.spark_extension.create_dataframe

.. autofunction:: nldsl.spark_extension.load_from

.. autofunction:: nldsl.spark_extension.save_to

.. autofunction:: nldsl.spark_extension.union

.. autofunction:: nldsl.spark_extension.difference

.. autofunction:: nldsl.spark_extension.intersection

.. autofunction:: nldsl.spark_extension.select_columns

.. autofunction:: nldsl.spark_extension.select_columns

.. autofunction:: nldsl.spark_extension.select_rows

.. autofunction:: nldsl.spark_extension.drop_columns

.. autofunction:: nldsl.spark_extension.join

.. autofunction:: nldsl.spark_extension.group_by

.. autofunction:: nldsl.spark_extension.replace_values

.. autofunction:: nldsl.spark_extension.append_column

.. autofunction:: nldsl.spark_extension.sort_by

.. autofunction:: nldsl.spark_extension.drop_duplicates

.. autofunction:: nldsl.spark_extension.rename_columns

.. autofunction:: nldsl.spark_extension.show

.. autofunction:: nldsl.spark_extension.show_schema

.. autofunction:: nldsl.spark_extension.describe

.. autofunction:: nldsl.spark_extension.head

.. autofunction:: nldsl.spark_extension.count


Code Generator
==============

.. autoclass:: nldsl.spark_extension.SparkCodeGenerator
   :show-inheritance:
   :members:


.. autoclass:: nldsl.spark_extension.SparkExpressionRule
   :show-inheritance:
   :members:
