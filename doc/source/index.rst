.. NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
..
.. NLDSL is licensed under a
.. Creative Commons Attribution-NonCommercial 3.0 Unported License.
..
.. You should have received a copy of the license along with this
.. work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

NLDSL documentation
*******************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   core
   spark_ext
   pandas_ext


Installation Instructions
=========================

This package is available on pip. To install it open a terminal and run::

    pip install nldsl


Quick Start
===========

.. automodule:: nldsl


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`