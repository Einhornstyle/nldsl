# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from nldsl import CodeGenerator, grammar
from nldsl.core import OperatorType
from nldsl.core import (KeywordRule, VariableRule, VarListRule, ExpressionRule,
                        GrammarParser, GrammarRule, GrammarFunction, PipeFunction,
                        Value, Keyword, Variable, Expression)




class TestBasicExpressions(unittest.TestCase):
    def setUp(self):
        self.er = ExpressionRule("test_expr", None)

    def test_or(self):
        input = ["x", "or", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_or(self):
        input = ["x", "and", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_not(self):
        input = ["not", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_boolean(self):
        input = ["x", "and", "y", "or", "not", "z"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_smaller(self):
        input = ["x", "<", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_larger(self):
        input = ["x", ">", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_eq(self):
        input = ["x", "==", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_not_eq(self):
        input = ["x", "!=", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_not_eq_v2(self):
        input = ["x", "<>", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_smaller_or_eq(self):
        input = ["x", "<=", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_larger_or_eq(self):
        input = ["x", ">=", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_in(self):
        input = ["x", "in", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_not_in(self):
        input = ["x", "not", "in", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_is(self):
        input = ["x", "is", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_is_not(self):
        input = ["x", "is", "not", "y"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_brackets(self):
        input = ["(", "x", ")"]
        self.assertEqual(self.er({}, input), ({self.er.name: "".join(input)}, []))

    def test_square_brackets(self):
        input = ["[", "x", "y", "z", "]"]
        target = "[x, y, z]"
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))




class TestCompoundExpressions(unittest.TestCase):
    def setUp(self):
        self.er = ExpressionRule("test_expr", None)

    def test_boolean(self):
        input = ["x", "and", "y", "or", "z"]
        eval_input = ["True", "and", "False", "or", "True"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))
        self.assertTrue(eval(self.er({}, eval_input)[0][self.er.name]))

    def test_boolean_with_not(self):
        input = ["not", "x", "and", "not", "not", "y", "or", "z"]
        eval_input = ["False", "and", "False", "or", "True"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))
        self.assertTrue(eval(self.er({}, eval_input)[0][self.er.name]))

    def test_boolean_with_comp(self):
        input = ["x", "<=", "5", "and", "y", ">", "1.2", "or", "not", "z", "!=", "None"]
        self.assertEqual(self.er({}, input), ({self.er.name: " ".join(input)}, []))

    def test_boolean_with_list(self):
        input1 = ["x", "is", "False", "or", "y", "!=", "abc", "and", "z", "not", "in"]
        input2 = ["[", "1.1", "ads", "1.0", "x", "]"]
        target = " ".join(input1) + " [1.1, ads, 1, x]"
        self.assertEqual(self.er({}, input1 + input2), ({self.er.name: target}, []))

    def test_arith(self):
        input = ["x", "+", "5", "*", "-", "y", "**", "2"]
        target = "x + 5 * -y**2"
        eval_input = ["2", "+", "5", "*", "-", "3", "**", "3"]
        eval_target = 2 + 5 * -3**3
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))
        self.assertEqual(eval(self.er({}, eval_input)[0][self.er.name]), eval_target)




class TestDyckExpressions(unittest.TestCase):
    def setUp(self):
        self.er = ExpressionRule("test_expr", None)

    def test_boolean(self):
        input = ["(", "x", "and", "y", ")", "or", "z"]
        target = "(x and y) or z"
        eval_input = ["(", "True", "and", "False", ")", "or", "not", "True"]
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))
        self.assertFalse(eval(self.er({}, eval_input)[0][self.er.name]))

    def test_boolean_with_not(self):
        input = ["not", "(", "x", "and", "not", "(", "not", "y", "or", "z", ")", ")"]
        target = "not (x and not (not y or z))"
        eval_input = ["not", "(", "True", "and", "not", "(", "not", "False", "or", "False", ")", ")"]
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))
        self.assertTrue(eval(self.er({}, eval_input)[0][self.er.name]))

    def test_boolean_with_comp(self):
        input = ["(", "x", "<=", "5", "and", "y", ">", "1.2", ")", "or", "not", "z", "!=", "None"]
        target = "(x <= 5 and y > 1.2) or not z != None"
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))

    def test_arith(self):
        input = ["(", "x", "+", "5", ")", "*", "-", "y", "**", "2"]
        target = "(x + 5) * -y**2"
        eval_input = ["(", "2", "+", "5", ")", "*", "-", "3", "**", "3"]
        eval_target = (2 + 5) * -3**3
        self.assertEqual(self.er({}, input), ({self.er.name: target}, []))
        self.assertEqual(eval(self.er({}, eval_input)[0][self.er.name]), eval_target)




class TestOperatorFunctions(unittest.TestCase):
    def setUp(self):
        class TestExprRule(ExpressionRule):
            def __init__(self, expr_name, next_keyword):
                super().__init__(expr_name, next_keyword)
                self.operator_map["and"] = "and"
                self.operator_map["=="] = "eq"
                self.operator_map["is not"] = ".is_not"

                self.operator_type["and"] = OperatorType.BINARY_FUNCTION
                self.operator_type["=="] = OperatorType.BINARY_FUNCTION
                self.operator_type["is not"] = OperatorType.UNARY_FUNCTION

        self.rule = TestExprRule("test expr", None)


    def test_and(self):
        input = ["not", "x", "and", "not", "y", "and", "z"]
        target = "and(and(not x, not y), z)"
        self.assertEqual(self.rule({}, input), ({self.rule.name: target}, []))


    def test_eq(self):
        input = ["x", "==", "y", "or", "z", "<", "5"]
        target = "eq(x, y) or z < 5"
        self.assertEqual(self.rule({}, input), ({self.rule.name: target}, []))


    def test_is_not(self):
        input = ["x", "is not", "y", "and", "y", "in", "[", "1", "2", "3", "]"]
        target = "and(x.is_not(y), y in [1, 2, 3])"
        self.assertEqual(self.rule({}, input), ({self.rule.name: target}, []))




class TestEmbeddedExpressions(unittest.TestCase):
    def test_grammar_rule_single(self):
        instructions = [Expression("expr")]
        gr = GrammarRule(instructions)
        input = ["(", "x", "+", "5", ")", "*", "-", "y", "**", "2"]
        target = {"expr": "(x + 5) * -y**2"}
        self.assertIsInstance(gr.rules[0], ExpressionRule)
        self.assertEqual(gr(input), target)


    def test_grammar_rule_multi(self):
        instructions = [Variable("var1"), Expression("expr"), Keyword("key"), Variable("var2")]
        gr = GrammarRule(instructions)
        input = ["xxx", "(", "x", "+", "5", ")", "*", "-", "y", "**", "2", "key", "yyy"]
        target = {"var1": "xxx", "expr": "(x + 5) * -y**2", "var2": "yyy"}
        self.assertIsInstance(gr.rules[0], VariableRule)
        self.assertIsInstance(gr.rules[1], ExpressionRule)
        self.assertIsInstance(gr.rules[2], KeywordRule)
        self.assertIsInstance(gr.rules[3], VariableRule)
        self.assertEqual(gr(input), target)


    def test_grammar_rule_custom_expr(self):
        class TestExprRule(ExpressionRule):
            def __init__(self, expr_name, next_keyword):
                super().__init__(expr_name, next_keyword)
                self.operator_map["and"] = " & "
                self.operator_map["not"] = "! "

        instructions = [Variable("var1"), Expression("expr"), Keyword("key"), Variable("var2")]
        gr = GrammarRule(instructions, expr_rule=TestExprRule)
        input = ["xxx", "(", "x", "and", "y", ")", "or", "not", "y", "**", "2", "key", "yyy"]
        target = {"var1": "xxx", "expr": "(x & y) or ! y**2", "var2": "yyy"}
        self.assertIsInstance(gr.rules[0], VariableRule)
        self.assertIsInstance(gr.rules[1], TestExprRule)
        self.assertIsInstance(gr.rules[2], KeywordRule)
        self.assertIsInstance(gr.rules[3], VariableRule)
        self.assertEqual(gr(input), target)


    def test_grammar_parser(self):
        my_grammar = """
            Grammar:
                group by $cols[$col] where !expr apply $agg
                agg := { min, max, avg, sum }
        """
        gp = GrammarParser(my_grammar)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {"agg": ["min", "max", "avg", "sum"]})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], ExpressionRule)
        self.assertIsInstance(gp.grammar_rules[3], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[4], VariableRule)
        input = ["col1", "col2", "col2", "where", "row1", "==", "row2", "apply", "max"]
        target = {"cols": ["col1", "col2", "col2"], "expr": "row1 == row2", "agg": "max"}
        self.assertEqual(gp(input), target)


    def test_grammar_function(self):
        def fun(code, args):
            """ This is my fun.
            Grammar:
                group by $cols[$col] where !expr apply $agg
                agg := { min, max, avg, sum }
            """
            return args

        input = ["col1", "col2", "col2", "where", "row1", "==", "row2", "apply", "max"]
        target = {"cols": ["col1", "col2", "col2"], "expr": "row1 == row2", "agg": "max"}

        gf = GrammarFunction(fun)
        self.assertEqual(gf.__doc__, fun.__doc__)
        self.assertEqual(gf("", input), target)


    def test_grammar_decorator(self):
        @grammar
        def fun(code, args):
            """ This is my fun.
            Grammar:
                group by $cols[$col] where !expr apply $agg
                agg := { min, max, avg, sum }
            """
            return args

        input = ["col1", "col2", "col2", "where", "row1", "==", "row2", "apply", "max"]
        target = {"cols": ["col1", "col2", "col2"], "expr": "row1 == row2", "agg": "max"}
        self.assertEqual(fun("", input), target)


    def test_pipe_function(self):
        instructions = [Variable("var1"), Expression("expr"), Keyword("key"), Variable("var2")]
        fun_list = [
            (lambda code, args, env: code + "[" + ", ".join(args) + "]",
             [Variable("var2"), Expression("expr"), Keyword("key2")]),
            (lambda code, args, env: code + "[" + ", ".join(args) + "]",
             [Variable("var1"), Value("val")])
        ]

        pf = PipeFunction("test pipe", instructions, fun_list)

        input = ["xxx", "(", "x", "+", "5", ")", "*", "-", "y", "**", "2", "key", "yyy"]
        target = "my_code[yyy, (x + 5) * -y**2, key2][xxx, val]"
        result = pf("my_code", input)
        self.assertEqual(result, target)


    def test_dsl_eval(self):
        @grammar
        def expr_fun(code, args):
            """ This is my fun.
            Grammar:
                group by expression $cols[$col] where !expr apply $agg
                agg := { min, max, avg, sum }
            """
            return code + " - {}, {}, {}".format(args["cols"], args["expr"], args["agg"])

        @grammar
        def on_dataframe(code, args):
            """Starts a pipeline on the given DataFrame.
        
            Examples:
                1. x = on df
                2. x = on df | transformer 1 ... | transformer n ... | operation
        
            Grammar:
                on $dataframe
        
            Args:
                dataframe (variable): The name of DataFrame
        
            Type:
                Initializer
            """
            return code + args["dataframe"]        

        input = "## x = on df | group by expression col1, col2, col3 where row1 == row2 apply max"
        target = "x = df - {}, {}, {}".format(["col1", "col2", "col3"], "row1 == row2", "max")
        cg = CodeGenerator()
        cg["group by expression"] = expr_fun
        cg["__infer__"] = on_dataframe
        self.assertEqual(cg(input, False)[0], target)


    def test_dsl_def(self):
        @grammar
        def expr_fun(code, args):
            """ This is my fun.
            Grammar:
                group by expression $cols[$col] where !expr apply $agg
                agg := { min, max, avg, sum }
            """
            return code + "{}, {}, {}".format(args["cols"], args["expr"], args["agg"])

        input = "#$ my expr !expr = group by expression col1, col2, col3 where !expr apply max"
        input2 = "## my expr row1 < row2"
        target = "{}, {}, {}".format(["col1", "col2", "col3"], "row1 < row2", "max")
        cg = CodeGenerator()
        cg["group by expression"] = expr_fun
        cg(input, False)

        self.assertTrue("my expr" in cg)
        self.assertEqual(cg(input2, False)[0], target)




if __name__ == "__main__":
    unittest.main(verbosity=2)
