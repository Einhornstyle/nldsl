# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from inspect import signature
from nldsl import CodeGenerator, grammar
from nldsl.core import (KeywordRule, VariableRule, VarListRule,
                        IdentityConverter, NameConverter, ListConverter, CodeMap,
                        GrammarParser, GrammarRule, GrammarFunction, PipeFunction,
                        Value, Keyword, Variable, VarList)




class TestKeywordRule(unittest.TestCase):
    def setUp(self):
        self.test_keyword = "xxx"
        self.kr = KeywordRule(self.test_keyword)

    def test_success(self):
        self.assertEqual(self.kr({"a": "x"}, [self.test_keyword]), ({"a": "x"}, []))

    def test_failure(self):
        self.assertRaises(ValueError, self.kr.__call__, {"a": "x"}, ["y"])




class TestVariableRule(unittest.TestCase):
    def test_success_no_choices(self):
        vr = VariableRule("var", {})
        self.assertEqual(vr({}, ["val"]), ({"var": "val"}, []))

    def test_success_with_choices(self):
        vr = VariableRule("var", {"var": ["1", "2", "3"]})
        self.assertEqual(vr({}, ["3"]), ({"var": "3"}, []))

    def test_failure_with_choices(self):
        vr = VariableRule("var", {"var": ["1", "2", "3"]})
        self.assertRaises(ValueError, vr.__call__, {}, ["0"])




class TestVarListRule(unittest.TestCase):
    def setUp(self):
        self.expr = [VariableRule("var1"), KeywordRule("key1"), VariableRule("var2"),
                     KeywordRule("key2"), KeywordRule("key3")]
        self.n_key = "next_key"
        self.input = ["x1", "key1", "x2", "key2", "key3", self.n_key]

    def test_success_with_name(self):
        vlr = VarListRule("var_list", self.expr, self.n_key)
        self.assertEqual(vlr({}, self.input), ({"var_list": [("x1", "x2")]}, [self.n_key]))

    def test_success_no_name(self):
        vlr = VarListRule("", self.expr, self.n_key)
        self.assertEqual(vlr({}, self.input), ({("var1", "var2"): [("x1", "x2")]}, [self.n_key]))

    def test_sucsess_choices_one(self):
        expr = [VariableRule("var1", {"var1": "xxx"}), KeywordRule("key1"), VariableRule("var2"),
                KeywordRule("key2"), KeywordRule("key3")]
        input = ["xxx", "key1", "x2", "key2", "key3", self.n_key]
        vlr = VarListRule("", expr, self.n_key)
        self.assertEqual(vlr({}, input), ({("var1", "var2"): [("xxx", "x2")]}, [self.n_key]))

    def test_failure_choices_one(self):
        expr = [VariableRule("var1", {"var1": "xxx"}), KeywordRule("key1"), VariableRule("var2"),
                KeywordRule("key2"), KeywordRule("key3")]
        vlr = VarListRule("", expr, self.n_key)
        self.assertRaises(ValueError, vlr.__call__, {}, self.input)

    def test_sucsess_choices_two(self):
        expr = [VariableRule("var1", {"var2": "yyy"}), KeywordRule("key1"),
                VariableRule("var2", {"var2": "yyy"}),
                KeywordRule("key2"), KeywordRule("key3")]
        input = ["x1", "key1", "yyy", "key2", "key3", self.n_key]
        vlr = VarListRule("", expr, self.n_key)
        self.assertEqual(vlr({}, input), ({("var1", "var2"): [("x1", "yyy")]}, [self.n_key]))




class TestGrammarRule(unittest.TestCase):
    def setUp(self):
        self.instructions1 = [
            Variable("var1"), Keyword("key1"), Keyword("key2"),
            Variable("var2"), Variable("var3"), Keyword("key3")
        ]
        self.instructions2 = [
            VarList("name", self.instructions1), Keyword("key4"), Variable("var4")
        ]


    def test_parse_rules(self):
        gr = GrammarRule(self.instructions1)

        self.assertIsInstance(gr.rules[0], VariableRule)
        self.assertIsInstance(gr.rules[1], KeywordRule)
        self.assertIsInstance(gr.rules[2], KeywordRule)
        self.assertIsInstance(gr.rules[3], VariableRule)
        self.assertIsInstance(gr.rules[4], VariableRule)
        self.assertIsInstance(gr.rules[5], KeywordRule)

        self.assertEqual(gr.rules[0].variable_name, "var1")
        self.assertEqual(gr.rules[1].keyword, "key1")
        self.assertEqual(gr.rules[2].keyword, "key2")
        self.assertEqual(gr.rules[3].variable_name, "var2")
        self.assertEqual(gr.rules[4].variable_name, "var3")
        self.assertEqual(gr.rules[5].keyword, "key3")


    def test_parse_choices(self):
        choices = {
            "var2": ["a1", "a2", "a3"],
            "var3": ["b1", "b2", "b3", "b4"]
        }
        gr = GrammarRule(self.instructions1, choices)
        self.assertEqual(gr.rules[3].choices, choices["var2"])
        self.assertEqual(gr.rules[4].choices, choices["var3"])
        self.assertEqual(gr.choices, choices)


    def test_call(self):
        choices = {
            "var2": ["a1", "a2", "a3"],
            "var3": ["b1", "b2", "b3", "b4"]
        }
        gr = GrammarRule(self.instructions1, choices)
        args = ["x1", "key1", "key2", "a2", "b3", "key3"]
        target = {"var1": "x1", "var2": "a2", "var3": "b3"}
        self.assertEqual(gr(args), target)


    def test_varlist(self):
        choices = {
            "var2": ["a1", "a2", "a3"],
            "var3": ["b1", "b2", "b3", "b4"]
        }
        gr = GrammarRule(self.instructions2, choices)

        self.assertIsInstance(gr.rules[0], VarListRule)
        self.assertIsInstance(gr.rules[1], KeywordRule)
        self.assertIsInstance(gr.rules[2], VariableRule)

        self.assertIsInstance(gr.rules[0].internal_rules[0], VariableRule)
        self.assertIsInstance(gr.rules[0].internal_rules[1], KeywordRule)
        self.assertIsInstance(gr.rules[0].internal_rules[2], KeywordRule)
        self.assertIsInstance(gr.rules[0].internal_rules[3], VariableRule)
        self.assertIsInstance(gr.rules[0].internal_rules[4], VariableRule)
        self.assertIsInstance(gr.rules[0].internal_rules[5], KeywordRule)

        self.assertEqual(gr.rules[0].internal_rules[0].variable_name, "var1")
        self.assertEqual(gr.rules[0].internal_rules[1].keyword, "key1")
        self.assertEqual(gr.rules[0].internal_rules[2].keyword, "key2")
        self.assertEqual(gr.rules[0].internal_rules[3].variable_name, "var2")
        self.assertEqual(gr.rules[0].internal_rules[4].variable_name, "var3")
        self.assertEqual(gr.rules[0].internal_rules[5].keyword, "key3")

        self.assertEqual(gr.choices, choices)




class TestGrammarParser(unittest.TestCase):
    def setUp(self):
        self.grammar1 = """
            Grammar:
                replace $old by $new
        """
        self.grammar2 = """
            Grammar:
                (replace values where) the $old by $new
        """
        self.grammar3 = """
            Grammar:
                group by $[$col] apply $agg
        """
        self.grammar4 = """
            Grammar:
                group by $column_list[$col] apply $agg
        """
        self.grammar5 = """
            Grammar:
                group by $[$col1 and $col2] apply $agg
        """
        self.grammar6 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
        """
        self.grammar7 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
        """
        self.grammar8 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
            Type:
                Function
        """
        self.grammar9 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
            Type:
                Initialization
        """
        self.grammar10 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
            Type:
                Transformation
        """
        self.grammar11 = """
            Grammar:
                group by $column_list[$col] apply $agg
                agg := { min, max, avg, sum }
            Type:
                Operation
        """

    def test_grammar_type(self):
        self.assertEqual(GrammarParser(self.grammar1).grammar_rule_type, None)
        self.assertEqual(GrammarParser(self.grammar8).grammar_rule_type, "Function")
        self.assertEqual(GrammarParser(self.grammar9).grammar_rule_type, "Initialization")
        self.assertEqual(GrammarParser(self.grammar10).grammar_rule_type, "Transformation")
        self.assertEqual(GrammarParser(self.grammar11).grammar_rule_type, "Operation")


    def test_grammar_simple(self):
        gp = GrammarParser(self.grammar1)
        self.assertEqual(gp.grammar_rule_name, "replace")
        self.assertEqual(gp.grammar_choices, {})
        self.assertIsInstance(gp.grammar_rules[0], VariableRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertEqual(gp(["x", "by", "y"]), {"old": "x", "new": "y"})


    def test_grammar_simple_with_brackets(self):
        gp = GrammarParser(self.grammar2)
        self.assertEqual(gp.grammar_rule_name, "replace values where")
        self.assertEqual(gp.grammar_choices, {})
        self.assertIsInstance(gp.grammar_rules[0], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[1], VariableRule)
        self.assertIsInstance(gp.grammar_rules[2], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[3], VariableRule)
        self.assertEqual(gp(["the", "x", "by", "y"]), {"old": "x", "new": "y"})


    def test_grammar_var_list(self):
        gp = GrammarParser(self.grammar3)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertEqual(gp(["x", "y", "z", "apply", "min"]),
                         {"col": ["x", "y", "z"], "agg": "min"})


    def test_grammar_var_list_named(self):
        gp = GrammarParser(self.grammar4)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertEqual(gp(["x", "y", "z", "apply", "min"]),
                         {"column_list": ["x", "y", "z"], "agg": "min"})


    def test_grammar_var_list_many(self):
        gp = GrammarParser(self.grammar5)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertEqual(gp(["x", "and", "y", "z", "and", "w", "apply", "min"]),
                         {("col1", "col2"): [("x", "y"), ("z", "w")], "agg": "min"})


    def test_grammar_var_list_choices_success(self):
        gp = GrammarParser(self.grammar6)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {"agg": ["min", "max", "avg", "sum"]})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertEqual(gp(["x", "y", "z", "apply", "min"]),
                         {"column_list": ["x", "y", "z"], "agg": "min"})


    def test_grammar_var_list_choices_failure(self):
        gp = GrammarParser(self.grammar7)
        self.assertEqual(gp.grammar_rule_name, "group by")
        self.assertEqual(gp.grammar_choices, {"agg": ["min", "max", "avg", "sum"]})
        self.assertIsInstance(gp.grammar_rules[0], VarListRule)
        self.assertIsInstance(gp.grammar_rules[1], KeywordRule)
        self.assertIsInstance(gp.grammar_rules[2], VariableRule)
        self.assertRaises(ValueError, gp.__call__, ["x", "y", "z", "apply", "inv"])




class TestGrammarFunction(unittest.TestCase):
    def test_docstring(self):
        def fun(code, args):
            """ This is my fun.
            Grammar:
                group by $cols[$col] apply $agg
                agg := { min, max, avg, sum }
            """
            return code, args

        gf = GrammarFunction(fun)
        self.assertEqual(gf.__doc__, fun.__doc__)

    def test_type(self):
        def fun(code, args):
            """ This is my fun.
            Grammar:
                group by $cols[$col] apply $agg
                agg := { min, max, avg, sum }
            Type:
                Transformation
            """
            return code, args

        gf = GrammarFunction(fun)
        self.assertEqual(gf.grammar_rule_type, "Transformation")




class TestGrammarDecorator(unittest.TestCase):
    def test_decorator(self):
        @grammar
        def fun(code, args):
            """ This is my fun.
            Grammar:
                group by $cols[$col] apply $agg
                agg := { min, max, avg, sum }
            """
            return code, args

        self.assertIsInstance(fun, GrammarFunction)
        self.assertEqual(fun("code", ["arg1", "apply", "avg"]),
                         ("code", {"cols": ["arg1"], "agg": "avg"}))


    def test_doc_propagation(self):
        target_doc =  "This is my fun. \nGrammar:"

        @grammar
        def fun(code, args):
            """This is my fun. \nGrammar:"""
            return code, args

        self.assertEqual(fun.__doc__, target_doc)


    def test_doc_propagation_2(self):
        target_doc =  "This is my fun. \nGrammar:"

        @grammar(doc=target_doc)
        def fun(code, args):
            return code, args

        self.assertEqual(fun.__doc__, target_doc)


    def test_decorator_with_args(self):
        doc = """ This is my fun.
        Grammar:
            group by $cols[$col] apply $agg
            agg := { min, max, avg, sum }
        """
        @grammar(doc)
        def fun(code, args):
            return code, args

        self.assertIsInstance(fun, GrammarFunction)
        self.assertEqual(fun("code", ["arg1", "apply", "avg"]),
                         ("code", {"cols": ["arg1"], "agg": "avg"}))


    def test_decorator_with_kwargs(self):
        test_doc = """ This is my fun.
        Grammar:
            group by $cols[$col] apply $agg
            agg := { min, max, avg, sum }
        """
        @grammar(doc=test_doc)
        def fun(code, args):
            return code, args

        self.assertIsInstance(fun, GrammarFunction)
        self.assertEqual(fun("code", ["arg1", "apply", "avg"]),
                         ("code", {"cols": ["arg1"], "agg": "avg"}))

    def test_type_propagation(self):
        doc = """ This is my fun.
        Grammar:
            group by $cols[$col] apply $agg
            agg := { min, max, avg, sum }
        Type:
            Operation
        """
        @grammar(doc)
        def fun(code, args):
            return code, args

        self.assertEqual(fun.grammar_rule_type, "Operation")




class TestIdentityConverter(unittest.TestCase):
    def test_identity(self):
        name = Value("my_name")
        ic = IdentityConverter(name, {})
        self.assertEqual(ic.value, "my_name")
        self.assertEqual(ic({}), ["my_name"])




class TestNameConverter(unittest.TestCase):
    def setUp(self):
        self.var = Variable("my_var")

    def test_success(self):
        vc = NameConverter(self.var, {"my_var"})
        self.assertEqual(vc.name, "my_var")
        self.assertEqual(vc({"my_var": 5}), [5])

    def test_failure(self):
        vc = NameConverter(self.var, {"my_var"})
        self.assertEqual(vc.name, "my_var")
        self.assertNotEqual(vc({"my_var": 7}), [5])

    def test_raises(self):
        self.assertRaises(ValueError, NameConverter.__init__, None, self.var, [])




class TestListConverter(unittest.TestCase):
    def setUp(self):
        self.vl = VarList("", [Variable("var1"), Keyword("key1"), Variable("var2"),
                               Variable("var3"), Value("val1")])
        self.i = [Variable("var2"), Keyword("key1"), Variable("var3"), Value("val1"),
                  Variable("var1")]


    def test_success(self):
        name = ("var2", "var3", "var1")
        name_dict = {name: self.i}
        input_dict = {name: [("a2", "a3", "a1"), ("b2", "b3", "b1")]}
        target = ["a1", "key1", "a2", "a3", "val1", "b1", "key1", "b2", "b3", "val1"]
        vlc = ListConverter(self.vl, name_dict)
        self.assertEqual(vlc.name, name)
        self.assertEqual(vlc(input_dict), target)


    def test_success_with_name(self):
        name = "test_name"
        self.vl.name = name
        name_dict = {name: self.i}
        input_dict = {name: [("a2", "a3", "a1"), ("b2", "b3", "b1")]}
        target = ["a1", "key1", "a2", "a3", "val1", "b1", "key1", "b2", "b3", "val1"]
        vlc = ListConverter(self.vl, name_dict)
        self.assertEqual(vlc.name, name)
        self.assertEqual(vlc(input_dict), target)




class TestPipeFunction(unittest.TestCase):
    def setUp(self):
        self.name = "test pipe function"
        self.instructions1 = [Variable("var1"), Keyword("key1"), Keyword("key2"),
                              Variable("var2"), Variable("var3"), Keyword("key3")]
        self.instructions2 = [VarList("", self.instructions1), Keyword("key4"), Variable("var4")]
        self.instructions3 = [Variable("var3"), Variable("var2"), Keyword("key1"), Variable("var1")]
        self.fun_list = [
            (lambda code, args, env: code + "[" + ", ".join(args) + "]",
             [VarList("", self.instructions3), Keyword("key4"), Variable("var4")]),
            (lambda code, args, env: code + "end", [])
        ]


    def test_extract_name_dict(self):
        pf = PipeFunction(self.name, self.instructions2, self.fun_list)
        result = pf._extract_name_dict(self.instructions2)
        target = {("var1", "var2", "var3"): self.instructions1, "var4": "var4"}
        self.assertEqual(result, target)


    def test_extract_converters(self):
        pf = PipeFunction(self.name, self.instructions2, self.fun_list)
        self.assertIsInstance(pf.argument_converters[0][1][0], ListConverter)
        self.assertIsInstance(pf.argument_converters[0][1][1], IdentityConverter)
        self.assertIsInstance(pf.argument_converters[0][1][2], NameConverter)
        self.assertEqual(pf.argument_converters[1][1], [])


    def test_call(self):
        pf = PipeFunction(self.name, self.instructions2, self.fun_list)
        input_list = [
            "a1", "key1", "key2", "a2", "a3", "key3",
            "b1", "key1", "key2", "b2", "b3", "key3",
            "key4", "x"
        ]
        target = "my_code[a3, a2, key1, a1, b3, b2, key1, b1, key4, x]end"
        result = pf("my_code", input_list)
        self.assertEqual(result, target)


    def test_type_none(self):
        g1 = "\n".join(["Grammar:", "\tg1"])
        g2 = "\n".join(["Grammar:", "\tg2", "Type:", "Function"])
        g3 = "\n".join(["Grammar:", "\tg3", "Type:", "Initialization"])
        g4 = "\n".join(["Grammar:", "\tg4", "Type:", "Transformation"])
        g5 = "\n".join(["Grammar:", "\tg5", "Type:", "Operation"])

        gf1 = GrammarFunction(lambda x: x, grammar_desc=g1)
        gf2 = GrammarFunction(lambda x: x, grammar_desc=g2)
        gf3 = GrammarFunction(lambda x: x, grammar_desc=g3)
        gf4 = GrammarFunction(lambda x: x, grammar_desc=g4)
        gf5 = GrammarFunction(lambda x: x, grammar_desc=g5)

        self.assertEqual(gf1.grammar_rule_type, None)
        self.assertEqual(gf2.grammar_rule_type, "Function")
        self.assertEqual(gf3.grammar_rule_type, "Initialization")
        self.assertEqual(gf4.grammar_rule_type, "Transformation")
        self.assertEqual(gf5.grammar_rule_type, "Operation")

        pf1 = PipeFunction("p1", [], [(gf1, []), (gf2, []), (gf3, [])])
        pf2 = PipeFunction("p2", [], [(gf4, []), (gf4, []), (gf4, [])])
        pf3 = PipeFunction("p3", [], [(gf2, []), (gf4, []), (gf4, [])])
        pf4 = PipeFunction("p4", [], [(gf3, []), (gf4, []), (gf5, [])])
        pf5 = PipeFunction("p5", [], [(gf4, []), (gf4, []), (gf5, [])])

        self.assertEqual(pf1.grammar_rule_type, None)
        self.assertEqual(pf2.grammar_rule_type, "Transformation")
        self.assertEqual(pf3.grammar_rule_type, "Function")
        self.assertEqual(pf4.grammar_rule_type, "Initialization")
        self.assertEqual(pf5.grammar_rule_type, "Operation")




class TestCodeMap(unittest.TestCase):
    def setUp(self):
        self.cm = CodeMap()

    def test_convert_name(self):
        name, _ = self.cm._convert_name("test name")
        self.assertEqual(name, self.cm.dynamic_prefix + "_test_name")

    def test_longest_name(self):
        empty = self.cm._compute_longest_name()
        self.assertEqual(empty, 0)
        self.assertEqual(empty, self.cm.longest_name)

        self.cm["test name"] = lambda code, args: code + args
        length = self.cm._compute_longest_name()
        self.assertEqual(length, 2)
        self.assertEqual(length, self.cm.longest_name)

    def test_setitem_getitem(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertTrue(hasattr(self.cm, self.cm.dynamic_prefix + "_test_name"))
        self.assertEqual(self.cm["test name"], f)

    def test_delitem(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        del self.cm["test name"]
        self.assertFalse(hasattr(self.cm, self.cm.dynamic_prefix + "_test_name"))

    def test_iter(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        for name in self.cm:
            self.assertEqual(name, "test name")

    def test_len(self):
        self.assertEqual(len(self.cm), 0)
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertEqual(len(self.cm), 1)

    def test_contains(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertTrue("test name" in self.cm)
        self.assertFalse("foo bar" in self.cm)

    def test_keys(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        for name in self.cm.keys():
            self.assertEqual(name, "test name")

    def test_values(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertTrue(f in self.cm.values())

    def test_items(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        for name, fun in self.cm.items():
            self.assertEqual(name, "test name")
            self.assertEqual(fun, f)

    def test_get(self):
        f = lambda code, args, env: code + args + env
        self.assertEqual(self.cm.get("test name"), None)
        self.assertEqual(self.cm.get("test name", "foo bar"), "foo bar")
        self.cm["test name"] = f
        self.assertEqual(self.cm.get("test name"), f)

    def test_eq(self):
        self.assertTrue(self.cm == CodeMap())
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertFalse(self.cm == CodeMap())

    def test_eq_2(self):
        cm_2 = CodeMap()
        self.assertTrue(self.cm == cm_2)
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertFalse(self.cm == cm_2)

    def test_ne(self):
        self.assertFalse(self.cm != CodeMap())
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertTrue(self.cm != CodeMap())

    def test_ne_2(self):
        cm_2 = CodeMap()
        self.assertFalse(self.cm != cm_2)
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertTrue(self.cm != cm_2)

    def test_pop(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertNotEqual(self.cm, CodeMap())
        fun = self.cm.pop("test name")
        self.assertEqual(self.cm, CodeMap())
        self.assertEqual(fun, f)

    def test_popitem(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertNotEqual(self.cm, CodeMap())
        fun = self.cm.popitem()
        self.assertEqual(self.cm, CodeMap())
        self.assertEqual(fun, ("test name", f))

    def test_clear(self):
        f = lambda code, args, env: code + args + env
        self.cm["test name"] = f
        self.assertNotEqual(self.cm, CodeMap())
        self.cm.clear()
        self.assertEqual(self.cm, CodeMap())

    def test_update(self):
        f = lambda code, args, env: code + args + env
        g = lambda code, args, env: code + args + env
        cm1, cm2, cm3, cm4 = CodeMap(), CodeMap(), CodeMap(), CodeMap()
        cm1["test name"] = f
        cm2["test name two"] = g

        cm3["test name"] = f
        cm3["test name two"] = g
        cm4["test name"] = f
        cm4["test name two"] = g

        cm1.update(cm2)
        self.assertFalse(cm1 == cm2)
        self.assertFalse(cm2 == cm1)
        self.assertTrue(cm4 == cm3)
        self.assertTrue(dict(cm1.items()) == dict(cm3.items()))
        self.assertTrue(cm1 == cm3)


    def test_setdefault(self):
        f = lambda code, args, env: code + args + env
        self.assertEqual(self.cm.get("test name"), None)
        fun = self.cm.setdefault("test name", f)
        self.assertTrue("test name" in self.cm)
        self.assertEqual(self.cm["test name"], f)
        self.assertEqual(fun, f)

    def test_register_function(self):
        f = lambda code, args, env: code + args + env
        g = lambda code, args, env: code + args + env
        CodeMap.register_function(f, "test name five")
        self.assertTrue("test name five" in self.cm)
        self.assertTrue(hasattr(CodeMap, self.cm.dynamic_prefix + "_test_name_five"))

        self.cm["test name six"] = g
        self.assertFalse(hasattr(CodeMap, self.cm.dynamic_prefix + "_test_name_six"))

    def test_request_function(self):
        h = lambda code, args, env: code + args + env
        CodeMap.register_function(h, "test name three")
        self.assertTrue(hasattr(CodeMap, self.cm.dynamic_prefix + "_test_name_three"))
        self.assertNotEqual(CodeMap.request_function("test name three"), None)

    def test_remove_function(self):
        i = lambda code, args, env: code + args + env
        CodeMap.register_function(i, "test name four")
        self.assertTrue(hasattr(CodeMap, self.cm.dynamic_prefix + "_test_name_four"))
        CodeMap.remove_function("test name four")
        self.assertFalse(hasattr(CodeMap, self.cm.dynamic_prefix + "_test_name_four"))




class TestCodeGenerator(unittest.TestCase):
    def test_dsl_eval(self):
        def fun(code, args):
            idx = args.index("subtract")
            add = " + ".join(args[:idx])
            sub = " - ".join(args[idx+1:])
            return code + "(" + add + " - " + sub + ")"

        cg = CodeGenerator()
        cg["add"] = fun
        input = "## add 1.8, 2, 3.12 subtract 4.4 5.1 6.1231"
        target = "(1.8 + 2 + 3.12 - 4.4 - 5.1 - 6.1231)"
        self.assertEqual(cg(input, False)[0], target)


    def test_dsl_def(self):
        l1 = lambda code, args: code + "!!!{}!!!".format(args[0])
        l2 = lambda code, args: code + "???{}???".format(args[0])

        cg = CodeGenerator()
        cg["lambda one"] = l1
        cg["lambda two"] = l2
        d = "#$ my pipeline fun $x, $y = lambda one $y | lambda two $x"
        input = "## my pipeline fun xxx yyy"
        target = "!!!yyy!!!???xxx???"
        self.assertEqual(cg(d +  input)[0], target)


    def test_register_dsl(self):
        l1 = lambda code, args: code + "!!!{}!!!".format(args[0])
        l2 = lambda code, args: code + "???{}???".format(args[0])
        d = "#$ my pipeline dsl fun $x, $y = lambda dsl one $y | lambda dsl two $x"
        input = "## my pipeline dsl fun xxx yyy"
        target = "!!!yyy!!!???xxx???"

        CodeGenerator.register_function(l1, "lambda dsl one")
        CodeGenerator.register_function(l2, "lambda dsl two")
        CodeGenerator.register_dsl(d)

        cg = CodeGenerator()
        self.assertEqual(cg(input)[0], target)


    def test_types(self):
        fun = lambda code, args: code + args[0]
        cg = CodeGenerator()
        cg["type test"] = fun
        sign_test = "## type test <="
        int_test = "## type test 7"
        float_test = "## type test 7.12"
        bool_test = "## type test True"
        string_test = "## type test 'teststring'"
        id_test = "## type test testid"

        self.assertEqual(cg(sign_test)[0], "<=")
        self.assertEqual(cg(int_test)[0], "7")
        self.assertEqual(cg(float_test)[0], "7.12")
        self.assertEqual(cg(bool_test)[0], "True")
        self.assertEqual(cg(string_test)[0], "'teststring'")
        self.assertEqual(cg(id_test)[0], "testid")


    def test_env_passing(self):
        fun = lambda code, env: code + "env=" + env["test_env"]
        cg = CodeGenerator(test_env="test_env")
        cg["environment test function"] = fun
        input = "## environment test function"
        self.assertEqual(cg(input)[0], "env=test_env")


    def test_infer(self):
        @grammar
        def fun(code):
            """
            Grammar:
                infer name test
            """
            return code + "infer name test"

        cg = CodeGenerator()
        cg["__infer__"] = fun
        self.assertEqual(cg["infer name test"], fun)
        self.assertEqual(cg("## infer name test")[0], "infer name test")


    def test_expr_only(self):
        @grammar
        def expression_only(code, args):
            """Parses python expressions outside of a pipeline.
        
            Examples:
                1. x = 5
                2. x = y % (5.7 + z / 2)
                3. x = w and (y or not z)
        
            Grammar:
                !expr
        
            Args:
                expr (expression): The expression to be evaluated.
        
            Type:
                Function
            """
            return code + args["expr"]
 
        cg = CodeGenerator()
        cg["__infer__"] = expression_only
        input = "## z = (x > 5) and y != 7"
        self.assertEqual(cg(input)[0], "z = (x > 5) and y != 7")


    def test_extract_env(self):
        cg = CodeGenerator()
        old_env = cg.env
        cg.extract_environment(["add", "ads", "asdg"])
        self.assertEqual(cg.env, old_env)




class TestInitMetaModel(unittest.TestCase):
    def test_init_meta_model(self):
        class MyCG(CodeGenerator): pass

        original_model = CodeGenerator.META_MODEL
        self.assertEqual(MyCG.META_MODEL, CodeGenerator.META_MODEL)

        MyCG.init_meta_model("//#", "//$")

        self.assertNotEqual(MyCG.META_MODEL, CodeGenerator.META_MODEL)
        self.assertEqual(original_model, CodeGenerator.META_MODEL)


    def test_init_meta_model_instance_eval(self):
        @grammar
        def fun(code):
            """
            Grammar:
                infer name test
            """
            return code + "infer name test"
        
        class MyCG(CodeGenerator): pass
        MyCG.init_meta_model("//#")

        cg = MyCG()
        cg["__infer__"] = fun

        self.assertEqual(cg["infer name test"], fun)
        self.assertEqual(cg("//# infer name test")[0], "infer name test")


    def test_init_meta_model_class_eval(self):
        @grammar
        def fun(code):
            """
            Grammar:
                class test
            """
            return code + "class test"

        class MyCG(CodeGenerator): pass
        MyCG.init_meta_model("//#")
       
        MyCG.register_function(fun)
        cg = MyCG()

        self.assertEqual(cg("//# class test")[0], "class test")


    def test_init_meta_model_instance_def(self):
        class MyCG(CodeGenerator): pass
        MyCG.init_meta_model("~#", "~$")

        l1 = lambda code, args: code + "!!!{}!!!".format(args[0])
        l2 = lambda code, args: code + "???{}???".format(args[0])

        cg = MyCG()
        cg["lambda one"] = l1
        cg["lambda two"] = l2

        d = "~$ my pipeline fun $x, $y = lambda one $y | lambda two $x"

        input = "~# my pipeline fun xxx yyy"
        target = "!!!yyy!!!???xxx???"

        self.assertEqual(cg(d +  input)[0], target)


    def test_init_meta_model_class_def(self):
        class MyCG(CodeGenerator): pass
        MyCG.init_meta_model("//#", "//$")

        l1 = lambda code, args: code + "!!!{}!!!".format(args[0])
        l2 = lambda code, args: code + "???{}???".format(args[0])

        MyCG.register_function(l1, "lambda one")
        MyCG.register_function(l2, "lambda two")
        cg = MyCG()

        d = "//$ my pipeline fun $x, $y = lambda one $y | lambda two $x"
        input = "//# my pipeline fun xxx yyy"
        target = "!!!yyy!!!???xxx???"

        self.assertEqual(cg(d +  input)[0], target)


    def test_init_meta_model_register_dsl(self):
        class MyCG(CodeGenerator): pass
        MyCG.init_meta_model("~#", "//$")

        l1 = lambda code, args: code + "!!!{}!!!".format(args[0])
        l2 = lambda code, args: code + "???{}???".format(args[0])
        d = "//$ my pipeline dsl fun $x, $y = lambda dsl one $y | lambda dsl two $x"
        input = "~# my pipeline dsl fun xxx yyy"
        target = "!!!yyy!!!???xxx???"

        MyCG.register_function(l1, "lambda dsl one")
        MyCG.register_function(l2, "lambda dsl two")
        MyCG.register_dsl(d)

        cg = MyCG()
        self.assertEqual(cg(input)[0], target)




if __name__ == "__main__":
    unittest.main(verbosity=2)
